import styled from "styled-components";

const SubtendButton = styled.button`
    background-color: #404D5D;
    color: ${props => props.cButton ? "red" : "#9DB2B9"};    ;    
    height: 60px;
    width: 60px;
    font-weight: 500;
    font-size: 22px;
    border: none;
    
`
const NumberButton = styled(SubtendButton)`
    color: white;
    font-size: 18px;
    font-weight: 600;
`

export { SubtendButton, NumberButton }