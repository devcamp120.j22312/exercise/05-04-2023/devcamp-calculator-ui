import {SubtendButton, NumberButton} from "./style/SubtendButton";


function App() {
  return (
    <div>
      <div style={{backgroundColor: "#394655", width: "250px", height: "480px", alignItems:"center", marginLeft:"50px", marginTop:"30px"}}>
        <div style={{height:"60px"}}></div>
        <hr style={{backgroundColor:"#9DB2B9", width: "85%"}} />
        <div style={{height: "90px"}}></div>

        <div style={{height:"60px", alignContent:"center"}}>
          <SubtendButton cButton>C</SubtendButton>
          <SubtendButton>≠</SubtendButton>
          <SubtendButton>%</SubtendButton>
          <SubtendButton>/</SubtendButton>
        </div>
        <div style={{height:"60px", alignContent:"center"}}>
          <NumberButton>7</NumberButton>
          <NumberButton>8</NumberButton>
          <NumberButton>9</NumberButton>
          <SubtendButton>x</SubtendButton>
        </div>
        <div style={{height:"60px", alignContent:"center"}}>
          <NumberButton>4</NumberButton>
          <NumberButton>5</NumberButton>
          <NumberButton>6</NumberButton>
          <SubtendButton>-</SubtendButton>
        </div>
        <div style={{height:"60px", alignContent:"center"}}>
          <NumberButton>1</NumberButton>
          <NumberButton>2</NumberButton>
          <NumberButton>3</NumberButton>
          <SubtendButton>+</SubtendButton>
        </div>
        <div style={{height:"60px", alignContent:"center"}}>
          <NumberButton>.</NumberButton>
          <NumberButton>0</NumberButton>
          <NumberButton>?</NumberButton>
          <SubtendButton>=</SubtendButton>
        </div>
      </div>
    </div>
  );
}

export default App;
